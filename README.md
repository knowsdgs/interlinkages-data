# SDG Interlikages Data

## Visualizations database

The goal and target level databases underlie the visualisations (chord, sankey) and table of descriptions in the platform; the interlinkages in the databases are the univocal links identified in each publication, univocal meaning that links with the same goalA-goalB or targetA-targetB, type, direction are aggregated into one row, and their geo scales and descriptions are merged into one cell respectively. 
Read more about the methodology in the technical report or feel free to reach out to the team.

## Advanced search

The advanced search databases contains all interlinkages recorded for each publication without any aggregation. 
Read more about the methodology in the technical report or feel free to reach out to the team.

## Support
For support or any feedback please contact us via email: JRC-SDGs@ec.europa.eu or create a new "issue" in this project.

## License
Licensed under the Creative Commons Attribution 4.0 International (CC BY 4.0) licence. Reuse is allowed if provided appropriate credit is given and changes are indicated.
